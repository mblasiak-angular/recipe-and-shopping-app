import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  @Output() recipeClicked = new EventEmitter<string>();
  @Output() shoppingListClicked = new EventEmitter<string>();

  onRecipesClicked() {
    this.recipeClicked.emit('recipe');
  }

  onShoppingListClicked() {
    this.shoppingListClicked.emit('shopping-list');
  }
}

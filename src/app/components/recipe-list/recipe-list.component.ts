import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Recipe} from '../../models/recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  @Output() recipeWasSelected = new EventEmitter<Recipe>();

  recipes: Recipe[] = [
    new Recipe('A test recipe', 'This is simply a test', 'https://www.przyslijprzepis.pl/uploads/media/recipe/0002/74/ce9069cdd20c0' +
      'd382959601c1a4a988a598e4fa4.jpeg'),
    new Recipe('test', 'test description', 'https://static.potreningu.pl/articles/2016/0929/5_article_57ecabed133cd_article_day_v3.jpg'),
    new Recipe('test', 'test description', 'https://pics.pyszne.pl/galeria_zdjec/fabryka-pizzy-krakow-jozefa-pizza-salami.jpg')
  ];

  constructor() {
  }

  ngOnInit() {
  }

  onRecipeSelected(recipe: Recipe) {
    this.recipeWasSelected.emit(recipe);
  }
}

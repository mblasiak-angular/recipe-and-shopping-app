import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Ingredient} from '../../models/ingredient.model';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {

  @Output()
  addIngredientWasClicked = new EventEmitter<Ingredient>();

  constructor() {
  }

  ngOnInit() {
  }

  addIngredient(nameInput: HTMLInputElement, amountInput: HTMLInputElement) {
    const newIngredientName = nameInput.value;
    const newIngredientAmount = parseFloat(amountInput.value);
    const newIngredient = new Ingredient(newIngredientName, newIngredientAmount);

    this.addIngredientWasClicked.emit(newIngredient);

  }
}

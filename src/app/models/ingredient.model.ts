export class Ingredient {
  name: string;
  amount: number;

  constructor(ingredientName: string, ingredientAmount: number) {
    this.name = ingredientName;
    this.amount = ingredientAmount;
  }
}
